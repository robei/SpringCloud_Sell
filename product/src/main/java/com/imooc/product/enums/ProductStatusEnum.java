package com.imooc.product.enums;

import lombok.Getter;

/**
 * Created by zghgchao 2018/6/14 21:33
 * 商品上下架状态
 */
@Getter
public enum ProductStatusEnum {

    UP(0,"在架"),
    DOWN(1,"下架"),
    ;

    private Integer code;
    private String message;

    ProductStatusEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
