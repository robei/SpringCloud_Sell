package com.imooc.order.repository;

import com.imooc.order.dataobject.OrderDetail;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by zghgchao 2018/6/17 8:54
 */
public interface OrderDetailRepository extends JpaRepository<OrderDetail,String> {
}
