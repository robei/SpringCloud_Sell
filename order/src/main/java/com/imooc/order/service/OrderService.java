package com.imooc.order.service;

import com.imooc.order.dto.OrderDTO;

/**
 * Created by zghgchao 2018/6/17 9:43
 */
public interface OrderService {
    /**
     * 创建订单
     * @param orderDTO
     * @return
     */
    OrderDTO create(OrderDTO orderDTO);
}
