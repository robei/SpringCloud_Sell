package com.imooc.order.utils;

import com.imooc.order.VO.ResultVO;

/**
 * Created by zghgchao 2018/6/14 22:25
 */
public class ResultVOUtil {

    public static ResultVO success(Object object){
        ResultVO resultVO = new ResultVO();
        resultVO.setCode(0);
        resultVO.setMsg("success");
        resultVO.setData(object);
        return resultVO;
    }
}
