package com.imooc.product.dto;

import lombok.Data;

/**
 * Created by zghgchao 2018/6/17 16:44
 */
@Data
public class CartDTO {

    /**商品id**/
    private String productId;

    /**商品数量**/
    private Integer productQuantity;

    public CartDTO() {
    }

    public CartDTO(String productId, Integer productQuantity) {
        this.productId = productId;
        this.productQuantity = productQuantity;
    }
}
