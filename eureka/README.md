该服务为注册中心（启动项目要先启动它）
### mvn打包
~~~
mvn clean package
~~~
### 运行
~~~
java -jar target/eureka-0.0.1-SNAPSHOT.jar
# 后台启动
nohub java -jar target/eureka-0.0.1-SNAPSHOT.jar > /dev/null 2>&1 &
# 查看进程
ps -ef | grep eureka
# 停止进程
kill -9 _进程号_
~~~