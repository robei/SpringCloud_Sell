package com.imooc.order.repository;

import com.imooc.order.dataobject.OrderMaster;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by zghgchao 2018/6/17 8:53
 */
public interface OrderMasterRepository extends JpaRepository<OrderMaster,String> {
}
